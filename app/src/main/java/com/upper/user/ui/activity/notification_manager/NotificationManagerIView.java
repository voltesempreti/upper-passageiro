package com.upper.user.ui.activity.notification_manager;

import com.upper.user.base.MvpView;
import com.upper.user.data.network.model.NotificationManager;

import java.util.List;

public interface NotificationManagerIView extends MvpView {

    void onSuccess(List<NotificationManager> notificationManager);

    void onError(Throwable e);

}