package com.upper.user.ui.fragment.dispute;

public interface DisputeCallBack {
    void onDisputeCreated();
}
