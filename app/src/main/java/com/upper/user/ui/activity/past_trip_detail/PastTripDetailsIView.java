package com.upper.user.ui.activity.past_trip_detail;

import com.upper.user.base.MvpView;
import com.upper.user.data.network.model.Datum;

import java.util.List;

public interface PastTripDetailsIView extends MvpView {

    void onSuccess(List<Datum> pastTripDetails);

    void onError(Throwable e);
}
