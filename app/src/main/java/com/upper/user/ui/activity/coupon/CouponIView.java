package com.upper.user.ui.activity.coupon;

import com.upper.user.base.MvpView;
import com.upper.user.data.network.model.PromoResponse;

public interface CouponIView extends MvpView {
    void onSuccess(PromoResponse object);

    void onError(Throwable e);
}
