package com.upper.user.ui.activity.profile;

import com.upper.user.base.MvpView;
import com.upper.user.data.network.model.User;

public interface ProfileIView extends MvpView {

    void onSuccess(User user);

    void onError(Throwable e);

}
