package com.upper.user.ui.activity.passbook;

import com.upper.user.base.MvpView;
import com.upper.user.data.network.model.WalletResponse;

public interface WalletHistoryIView extends MvpView {
    void onSuccess(WalletResponse response);

    void onError(Throwable e);

}
