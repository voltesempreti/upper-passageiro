package com.upper.user.ui.activity.login;

import com.upper.user.base.MvpView;
import com.upper.user.data.network.model.ForgotResponse;
import com.upper.user.data.network.model.Token;

public interface LoginIView extends MvpView {
    void onSuccess(Token token);

    void onSuccess(ForgotResponse object);

    void onError(Throwable e);
}
