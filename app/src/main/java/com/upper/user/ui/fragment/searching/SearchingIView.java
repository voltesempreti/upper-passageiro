package com.upper.user.ui.fragment.searching;

import com.upper.user.base.MvpView;

public interface SearchingIView extends MvpView {
    void onSuccess(Object object);

    void onError(Throwable e);
}
