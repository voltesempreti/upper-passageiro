package com.upper.user.ui.fragment.book_ride;

import com.upper.user.base.MvpView;
import com.upper.user.data.network.model.PromoResponse;


public interface BookRideIView extends MvpView {
    void onSuccess(Object object);

    void onError(Throwable e);

    void onSuccessCoupon(PromoResponse promoResponse);
}
