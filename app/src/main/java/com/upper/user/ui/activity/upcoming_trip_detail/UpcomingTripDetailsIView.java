package com.upper.user.ui.activity.upcoming_trip_detail;

import com.upper.user.base.MvpView;
import com.upper.user.data.network.model.Datum;

import java.util.List;

public interface UpcomingTripDetailsIView extends MvpView {

    void onSuccess(List<Datum> upcomingTripDetails);

    void onError(Throwable e);
}
