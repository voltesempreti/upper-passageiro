package com.upper.user.ui.activity.location_pick;

import com.upper.user.base.MvpPresenter;

public interface LocationPickIPresenter<V extends LocationPickIView> extends MvpPresenter<V> {
    void address();
}
