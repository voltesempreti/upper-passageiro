package com.upper.user.ui.activity.invite_friend;

import com.upper.user.base.MvpView;
import com.upper.user.data.network.model.User;

public interface InviteFriendIView extends MvpView {

    void onSuccess(User user);

    void onError(Throwable e);

}
