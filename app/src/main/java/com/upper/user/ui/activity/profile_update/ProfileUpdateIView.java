package com.upper.user.ui.activity.profile_update;

import com.upper.user.base.MvpView;
import com.upper.user.data.network.model.User;

public interface ProfileUpdateIView extends MvpView {

    void onSuccess(User user);

    void onUpdateSuccess(User user);

    void onError(Throwable e);

    void onSuccessPhoneNumber(Object object);

    void onVerifyPhoneNumberError(Throwable e);

}
