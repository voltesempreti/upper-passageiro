package com.upper.user.ui.activity.help;

import com.upper.user.base.MvpView;
import com.upper.user.data.network.model.Help;

public interface HelpIView extends MvpView {

    void onSuccess(Help help);

    void onError(Throwable e);
}
