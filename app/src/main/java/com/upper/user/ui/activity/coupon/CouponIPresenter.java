package com.upper.user.ui.activity.coupon;

import com.upper.user.base.MvpPresenter;

public interface CouponIPresenter<V extends CouponIView> extends MvpPresenter<V> {
    void coupon();
}
