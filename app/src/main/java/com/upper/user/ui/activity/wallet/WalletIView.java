package com.upper.user.ui.activity.wallet;

import com.upper.user.base.MvpView;
import com.upper.user.data.network.model.AddWallet;
import com.upper.user.data.network.model.WalletResponse;

public interface WalletIView extends MvpView {
    void onSuccess(AddWallet object);
    void onError(Throwable e);
    void onSuccess(WalletResponse response);
}
