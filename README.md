# MOBUB REVAMP NEW


##Installation Guide

```
git clone [PROJECT]
```

Delete ```.git``` folder in Project folder

Rename Package using refactor


## Getting Started

Build.gradle
```
applicationId "br.vaija.user"
buildConfigField "String", "BASE_URL", '"http://app.mobub.com.br/"'
buildConfigField "String", "CLIENT_SECRET", '"key"'
buildConfigField "String", "CLIENT_ID", '"2"'
```


string.xml
```
<string name="app_name" translatable="false">Mobub User</string>
<string name="google_map_key" translatable="false">key</string>
<string name="google_signin_server_client_id" translatable="false">key</string>
```

[google_signin_server_client_id](https://console.cloud.google.com/) -API&Services -> Credentials -> OAuth 2.0 client IDs -> Web client (auto created by Google Service)



Add app in [firebase](http://console.firebase.google.com/), and don't forgot to add SHA-1

[Firebase console](http://console.firebase.google.com/) -Realtime Database -> set database rules as below
```
{
  "rules": {
    ".read": true,
    ".write": true
  }
}
```



